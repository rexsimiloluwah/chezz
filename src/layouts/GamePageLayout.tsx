import { ThemeSwitch } from '@/components/shared';
import React, { PropsWithChildren } from 'react';

const GamePageLayout: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <main>
      <ThemeSwitch />
      {children}
    </main>
  );
};

export default GamePageLayout;
