import { ThemeSwitch } from '@/components/shared';
import React, { PropsWithChildren } from 'react';
import styled from 'styled-components';
import { Toaster } from 'react-hot-toast';

const CenterPageLayout: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <Layout>
      <Toaster />
      <ThemeSwitch />
      {children}
    </Layout>
  );
};

const Layout = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  margin: auto;
`;

export default CenterPageLayout;
