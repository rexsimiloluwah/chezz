import React, {
  createContext,
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  useContext
} from 'react';
import { useState } from 'react';

export type Theme = 'light' | 'dark';

interface IThemeStateContextProps {
  theme: Theme;
  setTheme: Dispatch<SetStateAction<Theme>>;
  updateTheme: (theme: Theme) => void;
}

const ThemeStateContext = createContext({} as IThemeStateContextProps);

export const ThemeStateProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const [theme, setTheme] = useState<Theme>((localStorage.getItem('theme') || 'dark') as Theme);

  const updateTheme = (theme: Theme) => {
    setTheme(theme);
    localStorage.setItem('theme', theme);
  };

  return (
    <ThemeStateContext.Provider value={{ theme, setTheme, updateTheme }}>
      {children}
    </ThemeStateContext.Provider>
  );
};

export const useThemeStateContext = () => useContext(ThemeStateContext);
