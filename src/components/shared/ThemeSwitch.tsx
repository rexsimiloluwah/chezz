import { Theme, useThemeStateContext } from '@/context';
import React from 'react';
import styled from 'styled-components';

const ThemeSwitch: React.FC = () => {
  const { theme, updateTheme } = useThemeStateContext();
  return (
    <ThemeSwitchContainer>
      <select
        name="select_theme"
        id="select_theme"
        onChange={({ target: { value } }) => updateTheme(value as Theme)}
        defaultValue={theme}
      >
        <option value="light">Theme: Light</option>
        <option value="dark">Theme: Dark</option>
      </select>
    </ThemeSwitchContainer>
  );
};

export default ThemeSwitch;

const ThemeSwitchContainer = styled.div`
  position: absolute;
  bottom: 10px;
  right: 10px;
  z-index: 99;

  select {
    padding: 0.2rem;
  }
`;
