import React, { PropsWithChildren } from 'react';
import ReactModal from 'react-modal';

export type OverlayProps = {
  isOpen: boolean;
};

const Overlay: React.FC<PropsWithChildren<OverlayProps>> = ({ isOpen, children }) => {
  return (
    <ReactModal
      isOpen={isOpen}
      style={{
        content: {
          width: 'fit-content',
          height: 'fit-content',
          background: 'transparent',
          margin: 'auto',
          padding: 0,
          border: 'none'
        },
        overlay: {
          background: 'rgba(0,0,0,0.1)',
          backdropFilter: 'blur(2px)'
        }
      }}
      ariaHideApp={false}
    >
      {children}
    </ReactModal>
  );
};

export default Overlay;
