import React, { PropsWithChildren } from 'react';
import ReactModal from 'react-modal';
import styled from 'styled-components';
import { HiOutlineX } from 'react-icons/hi';

export interface IModalProps {
  title?: string;
  isOpen: boolean;
  onAfterOpen?: () => void;
  onAfterClose?: () => void;
  onRequestClose: () => void;
}

const Modal: React.FC<PropsWithChildren<IModalProps>> = ({
  title,
  isOpen,
  onAfterClose,
  onAfterOpen,
  onRequestClose,
  children
}) => {
  return (
    <ReactModal
      isOpen={isOpen}
      onAfterOpen={onAfterOpen}
      onAfterClose={onAfterClose}
      onRequestClose={onRequestClose}
      style={{
        content: {
          width: 'fit-content',
          height: 'fit-content',
          background: 'transparent',
          margin: 'auto',
          padding: 0,
          border: 'none'
        },
        overlay: {
          background: 'rgba(0,0,0,0.1)',
          backdropFilter: 'blur(2px)'
        }
      }}
      ariaHideApp={false}
    >
      <ModalContainer>
        <div className="header">
          <div>{title}</div>
          <ModalCancelButton onClick={onRequestClose}>
            <HiOutlineX />
          </ModalCancelButton>
        </div>
        <div className="body">{children}</div>
      </ModalContainer>
    </ReactModal>
  );
};

export default Modal;

const ModalContainer = styled.div`
  border-radius: 32px;
  background: ${({ theme }) => theme.colors.bgPrimary};
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  min-width: 400px;

  > .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid ${({ theme }) => theme.colors.borderPrimary};
    padding: 0.5rem 1rem;
  }

  > .body {
    padding: 1rem;
  }
`;

export const ModalCancelButton = styled.button`
  display: flex;
  justify-content: center;
  cursor: pointer;
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  background: ${({ theme }) => theme.colors.bgSecondary};
  font-size: 24px;
  padding: 0.2rem;
  border-radius: 50%;
  color: ${({ theme }) => theme.colors.textPrimary};
`;
