import { DefaultTheme } from '@/styles/theme';
import React, { PropsWithChildren } from 'react';
import { Oval } from 'react-loader-spinner';
import styled, { useTheme } from 'styled-components';

const PagePreloader: React.FC<PropsWithChildren> = ({ children }) => {
  const theme = useTheme() as DefaultTheme;
  return (
    <PagePreloaderContainer>
      {children || (
        <Oval
          width={50}
          height={50}
          ariaLabel="page_preloader_spinner"
          color={theme.colors.bgSecondary}
          secondaryColor={theme.colors.bgPrimary}
          strokeWidth={3}
        />
      )}
    </PagePreloaderContainer>
  );
};

export default PagePreloader;

const PagePreloaderContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  margin: auto;
`;
