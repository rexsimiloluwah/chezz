import React, { PropsWithChildren } from 'react';
import styled from 'styled-components';
import Loader from './Loader';

type ButtonProps = {
  name: string;
  label?: string;
  isLoading?: boolean;
  variant?: 'default' | 'primary' | 'secondary' | 'warning' | 'info' | 'danger' | 'google';
  onClick?: () => void;
};

const Button: React.FC<
  PropsWithChildren<ButtonProps> & React.ButtonHTMLAttributes<HTMLButtonElement>
> = ({ name, isLoading, label, children, variant, ...props }) => {
  return (
    <StyledButton name={name} title={name} isLoading={isLoading} variant={variant} {...props}>
      {label || children}
      {isLoading ? <Loader /> : ''}
    </StyledButton>
  );
};

export default Button;

Button.defaultProps = {
  isLoading: false,
  variant: 'default'
};

const StyledButton = styled.button<Omit<ButtonProps, 'label'>>`
  display: flex;
  justify-content: center;
  align-items: center;
  pointer-events: ${({ isLoading }) => (isLoading ? 'none' : '')};
  cursor: ${({ isLoading, disabled }) => (isLoading || disabled ? 'not-allowed' : 'pointer')};
  outline: none;
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  font-size: 16px;
  opacity: ${({ isLoading, disabled }) => (isLoading || disabled ? '.5' : '.92')};
  transition: 0.2s ease-in-out;
  background: ${({ variant, theme }) =>
    variant === 'default' || !variant ? theme.colors.bgSecondary : theme.colors[variant]};
  border-radius: 12px;
  padding: 1rem;
  gap: 0.4rem;
  color: ${({ theme }) => theme.colors.textPrimary};
  font-weight: 400;

  &:hover {
    opacity: 1;
    transform: translateY(4px);
    font-weight: 600;
  }
`;
