import React, { PropsWithChildren } from 'react';
import { HiOutlineX } from 'react-icons/hi';
import ReactModal from 'react-modal';
import styled from 'styled-components';
import { ModalCancelButton } from './Modal';

export type SideDrawerProps = {
  title?: string;
  isOpen: boolean;
  onAfterOpen?: () => void;
  onAfterClose?: () => void;
  onRequestClose: () => void;
};

const SideDrawer: React.FC<PropsWithChildren<SideDrawerProps>> = ({
  title,
  isOpen,
  onAfterOpen,
  onAfterClose,
  onRequestClose,
  children
}) => {
  return (
    <ReactModal
      isOpen={isOpen}
      onAfterOpen={onAfterOpen}
      onAfterClose={onAfterClose}
      onRequestClose={onRequestClose}
      style={{
        content: {
          top: '0px',
          left: '0px',
          position: 'relative',
          margin: 'auto',
          border: 'none',
          background: 'transparent',
          borderRadius: 0,
          height: '100vh',
          transition: '.3s ease-in-out'
        },
        overlay: {
          background: 'rgba(0,0,0,0.1)',
          backdropFilter: 'blur(2px)',
          zIndex: 999
        }
      }}
      ariaHideApp={false}
    >
      <SideDrawerContainer>
        <div className="header">
          <h3>{title}</h3>
          <ModalCancelButton onClick={onRequestClose}>
            <HiOutlineX />
          </ModalCancelButton>
        </div>
        <div className="body">{children}</div>
      </SideDrawerContainer>
    </ReactModal>
  );
};

export default SideDrawer;

const SideDrawerContainer = styled.div`
  position: absolute;
  background: ${({ theme }) => theme.colors.bgPrimary};
  top: 0;
  right: 0;
  width: 35%;
  height: 100%;
  z-index: 999;

  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid ${({ theme }) => theme.colors.borderPrimary};
    padding: 1rem;
  }

  .body {
    overflow-y: scroll;
    padding: 1rem;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    width: 60%;
  }
`;
