import React from 'react';
import styled from 'styled-components';

type FormSelectProps = {
  name: string;
  options: Array<{ value: string; label: string }>;
  onChange: React.ChangeEventHandler<HTMLSelectElement>;
};

const FormSelect: React.FC<FormSelectProps & React.SelectHTMLAttributes<HTMLSelectElement>> = ({
  name,
  options,
  onChange,
  ...props
}) => {
  return (
    <StyledFormSelect name={name} title={name} onChange={onChange} {...props}>
      {options.map(({ value, label }, i) => (
        <option key={i} value={value}>
          {label}
        </option>
      ))}
    </StyledFormSelect>
  );
};

export default FormSelect;

const StyledFormSelect = styled.select`
  padding: 1rem;
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  border-radius: 12px;
`;
