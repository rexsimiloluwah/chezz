import { DefaultTheme } from '@/styles/theme';
import React from 'react';
import { ThreeDots } from 'react-loader-spinner';
import { useTheme } from 'styled-components';

type LoaderProps = {
  width?: number;
  height?: number;
};

const Loader: React.FC<LoaderProps> = ({ width, height }) => {
  const theme = useTheme() as DefaultTheme;
  return (
    <ThreeDots
      width={width || 32}
      height={height || 32}
      color={theme.colors.textPrimary}
      ariaLabel="page_preloader_spinner"
    />
  );
};

export default Loader;
