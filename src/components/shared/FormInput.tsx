import React from 'react';
import styled from 'styled-components';

type FormInputProps = {
  name: string;
  placeholder: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
} & React.InputHTMLAttributes<HTMLInputElement>;

const FormInput: React.FC<FormInputProps> = ({ name, placeholder, onChange, ...props }) => {
  return (
    <StyledFormInput
      name={name}
      title={name}
      onChange={onChange}
      placeholder={placeholder}
      {...props}
    />
  );
};

export default FormInput;

const StyledFormInput = styled.input`
  box-shadow: none;
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  border-radius: 12px;
  padding: 1rem;
`;
