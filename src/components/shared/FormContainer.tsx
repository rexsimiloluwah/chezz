import React, { PropsWithChildren } from 'react';
import styled from 'styled-components';

const FormContainer: React.FC<PropsWithChildren> = ({ children }) => {
  return <StyledFormContainer>{children}</StyledFormContainer>;
};

export default FormContainer;

const StyledFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 1rem;

  input,
  button {
    width: 100%;
  }
`;
