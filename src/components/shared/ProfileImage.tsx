import React from 'react';
import styled from 'styled-components';

type ProfileImageProps = {
  username?: string;
  imgSrc: string | null;
};

const ProfileImage: React.FC<ProfileImageProps> = ({ username, imgSrc }) => {
  if (imgSrc) {
    return (
      <ProfileImageContainer>
        <img src={imgSrc} alt="picture" />
      </ProfileImageContainer>
    );
  } else if (username) {
    return (
      <ProfileImageContainer>
        <h3>{username.charAt(0).toUpperCase()}</h3>
      </ProfileImageContainer>
    );
  }
  // default profile image
  return (
    <ProfileImageContainer>
      <h3>U</h3>
    </ProfileImageContainer>
  );
};

export default ProfileImage;

const ProfileImageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background: ${({ theme }) => theme.colors.warning};

  img {
    width: 100%;
    height: 100%;
  }
`;
