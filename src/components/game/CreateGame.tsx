import React, { useState } from 'react';
import styled from 'styled-components';
import { Button } from '../shared';
import { IModalProps } from '../shared/Modal';
import { Modal } from '../shared';
import StartGame from './StartGame';

const StartGameModal: React.FC<IModalProps> = ({ ...props }) => {
  return (
    <Modal {...props}>
      <StartGame />
    </Modal>
  );
};

const CreateGame = () => {
  const [isStartGameModalOpen, setIsStartGameModalOpen] = useState<boolean>(false);

  return (
    <CreateGameContainer>
      <StartGameModal
        isOpen={isStartGameModalOpen}
        onRequestClose={() => setIsStartGameModalOpen(false)}
        onAfterClose={() => {
          console.log('closed');
        }}
      />
      <div className="header">
        <h1>Create New Game</h1>
        <p>Create a new {'"chezz"'} game</p>
      </div>
      <div className="body">
        <Button
          name="create_game"
          label="Create Game"
          onClick={() => setIsStartGameModalOpen(true)}
        />
      </div>
    </CreateGameContainer>
  );
};

export const CreateGameContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 40%;
  background: ${({ theme }) => theme.colors.bgPrimary};
  padding: 2rem;
  border-radius: 32px;
  gap: 2rem;
  box-shadow: 0 10px 25px ${({ theme }) => theme.colors.bgPrimary};
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};

  .header {
    display: flex;
    flex-direction: column;
    gap: 1rem;
    text-align: center;

    p {
      color: ${({ theme }) => theme.colors.textSecondary};
    }
  }

  .body {
    width: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    text-align: center;
    gap: 1rem;
    button {
      width: 100%;
    }
  }

  @media (max-width: 600px) {
    width: 90%;
  }
`;

export default CreateGame;
