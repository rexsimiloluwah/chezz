import { PIECE_SELECT_OPTIONS } from '@/constants';
import { StartingPiece } from '@/types';
import { multiPlayerGame } from '@/utils/chessgame';
import { auth } from '@/utils/firebase';
import { Color } from 'chess.js';
import React, { useState } from 'react';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import { Button, FormSelect } from '../shared';

const StartGame: React.FC = () => {
  const { currentUser } = auth;
  const navigate = useNavigate();
  const [selectedPiece, setSelectedPiece] = useState<StartingPiece>('random');
  const [loading, setLoading] = useState<boolean>(false);

  const handleStartGame = async () => {
    setLoading(true);
    // compute the player's starting piece
    const startingPiece = (
      selectedPiece === 'random' ? ['b', 'w'][Math.round(Math.random())] : selectedPiece
    ) as Color;

    // creator
    const newMember = {
      uid: currentUser?.uid as string,
      piece: startingPiece,
      name: (currentUser?.displayName || localStorage.getItem('username')) as string,
      isCreator: true,
      score: 0,
      profileImage: currentUser?.photoURL || null
    };

    // create the new game
    await multiPlayerGame
      .create(newMember)
      .then((gameId) => {
        setLoading(false);
        toast.success('New game created successfully!');
        // navigate to the game page
        navigate(`/game/${gameId}`);
      })
      .catch((error) => {
        setLoading(false);
        toast.error(
          `Failed to create new game: ${
            error.message || error.error || 'An unknown error occurred'
          }`
        );
      });
  };

  return (
    <StartGameContainer>
      <div className="header">
        <h1>Select Piece</h1>
        <p>Select your preferred chess piece color to start the game.</p>
      </div>
      <div className="body">
        <FormSelect
          name="select_piece"
          options={PIECE_SELECT_OPTIONS}
          onChange={({ target: { value } }) => setSelectedPiece(value as StartingPiece)}
          defaultValue={'random'}
        />
        <Button
          name="start_game"
          label="Start Game"
          onClick={handleStartGame}
          isLoading={loading}
        />
      </div>
    </StartGameContainer>
  );
};

export default StartGame;

const StartGameContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 2rem;

  .header,
  .body {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    gap: 1rem;
    width: 100%;
  }

  .body select,
  button {
    width: 100%;
  }
`;
