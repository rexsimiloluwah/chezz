import { GameMember, GameOverReason } from '@/types';
import { parseGameOverReason } from '@/utils/chessgame';
import React from 'react';
import styled from 'styled-components';

export type GameOverProps = {
  isMultiPlayer: boolean;
  isWinner?: boolean;
  winner: Pick<GameMember, 'name' | 'piece'> | null;
  reason: GameOverReason;
};

const GameOver: React.FC<GameOverProps> = ({ isWinner, reason, isMultiPlayer, winner }) => {
  const { label, emoji } = parseGameOverReason(reason);
  return (
    <GameOverContainer>
      <h1>Game Over</h1>
      <h3>
        {label} {emoji}
      </h3>
      {!isMultiPlayer && reason === 'checkmate' && <h1>{winner?.name} Wins</h1>}
      {isMultiPlayer && reason === 'checkmate' && (isWinner ? <h1>You Win</h1> : <h1>You Lose</h1>)}
    </GameOverContainer>
  );
};

export default GameOver;

const GameOverContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 2rem;
`;
