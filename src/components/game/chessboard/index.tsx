import { BOARD_LETTERS_B, BOARD_LETTERS_W, BOARD_NUMBERS_B, BOARD_NUMBERS_W } from '@/constants';
import { ChessBoard } from '@/types';
import { getBoardPosition, isSquareDark } from '@/utils/chessboard';
import { Color } from 'chess.js';
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Piece from './Piece';
import Square from './Square';

type ChessBoardProps = {
  board: ChessBoard;
  turn: Color;
  isMultiPlayer: boolean;
};

const Board: React.FC<ChessBoardProps> = ({ board, turn, isMultiPlayer }) => {
  const [currBoard, setCurrBoard] = useState<FlatArray<ChessBoard, 0>>();

  useEffect(() => {
    setCurrBoard(turn === 'w' ? board.flat() : board.flat().reverse());
  }, [board, turn]);

  const boardLetters = turn === 'w' ? BOARD_LETTERS_W : BOARD_LETTERS_B;
  const boardNumbers = turn === 'w' ? BOARD_NUMBERS_W : BOARD_NUMBERS_B;

  return (
    <ChessBoardContainer>
      <MainChessBoard>
        <div className="board__border horizontal top">
          {boardLetters.map((char, id) => (
            <div key={id}>{char}</div>
          ))}
        </div>
        <div className="board__border horizontal bottom">
          {boardLetters.map((char, id) => (
            <div key={id}>{char}</div>
          ))}
        </div>
        <div className="board__border vertical left">
          {boardNumbers.map((char, id) => (
            <div key={id}>{char}</div>
          ))}
        </div>
        <div className="board__border vertical right">
          {boardNumbers.map((char, id) => (
            <div key={id}>{char}</div>
          ))}
        </div>
        {currBoard?.flat().map((piece, i) => (
          <Square
            position={getBoardPosition(i, turn)}
            key={i}
            isDark={isSquareDark(i)}
            isMultiPlayer={isMultiPlayer}
          >
            {piece && <Piece piece={piece} position={getBoardPosition(i, turn)} />}
          </Square>
        ))}
      </MainChessBoard>
    </ChessBoardContainer>
  );
};

export default Board;

const ChessBoardContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

const MainChessBoard = styled.div`
  position: relative;
  width: 90%;
  height: 90%;
  display: flex;
  flex-wrap: wrap;

  .board__border {
    position: absolute;
    display: flex;
    background: ${({ theme }) => theme.colors.bgPrimary};
    border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  }

  .board__border > div {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 10px;
  }

  .board__border.horizontal {
    width: 100%;
  }

  .board__border.horizontal > div {
    width: 12.5%;
    height: 20px;
  }

  .board__border.vertical {
    flex-direction: column;
    height: 100%;
  }

  .board__border.vertical > div {
    width: 20px;
    height: 12.5%;
  }

  .board__border.top {
    top: -21px;
    border-top-right-radius: 5px;
    border-top-left-radius: 5px;
  }

  .board__border.bottom {
    bottom: -21px;
    border-bottom-right-radius: 5px;
    border-bottom-left-radius: 5px;
  }

  .board__border.left {
    left: -21px;
    border-bottom-left-radius: 5px;
    border-top-left-radius: 5px;
  }

  .board__border.right {
    right: -21px;
    border-bottom-right-radius: 5px;
    border-top-right-radius: 5px;
  }
`;
