import { ChessPiece } from '@/types';
import React from 'react';
import { DragPreviewImage, useDrag } from 'react-dnd';
import styled from 'styled-components';

type BoardPieceProps = {
  piece: ChessPiece | null;
  position: string;
};

const Piece: React.FC<BoardPieceProps> = ({ piece, position }) => {
  const [{ isDragging }, drag, preview] = useDrag(
    () => ({
      type: 'piece',
      item: { id: `${position}_${piece?.type}_${piece?.color}` },
      collect: (monitor) => ({
        isDragging: !!monitor.isDragging()
      })
    }),
    [position]
  );

  const pieceName = `${piece?.type}_${piece?.color}`;
  const pieceImg = `/assets/chesspieces/${pieceName}.png`;

  return (
    <>
      <DragPreviewImage connect={preview} src={pieceImg} />
      <PieceContainer ref={drag} style={{ opacity: isDragging ? 0 : 1 }}>
        <img src={pieceImg} alt={pieceName} />
      </PieceContainer>
    </>
  );
};

export default Piece;

const PieceContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  cursor: grab;

  img {
    max-width: 70%;
    max-height: 70%;
  }
`;
