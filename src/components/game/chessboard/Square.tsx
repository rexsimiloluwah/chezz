import { singlePlayerGame, multiPlayerGame } from '@/utils/chessgame';
import React, { PropsWithChildren } from 'react';
import { useDrop } from 'react-dnd';
import styled from 'styled-components';

type BoardSquareProps = {
  isDark: boolean;
  position: string;
  isMultiPlayer: boolean;
};

const Square: React.FC<PropsWithChildren<BoardSquareProps>> = ({
  isDark,
  isMultiPlayer,
  position,
  children
}) => {
  const [, drop] = useDrop<{ id: string }>({
    accept: 'piece',
    drop: async (item) => {
      if (isMultiPlayer) {
        await multiPlayerGame.handleMove(item.id.split('_')[0], position);
        return;
      } else {
        singlePlayerGame.handleMove(item.id.split('_')[0], position);
      }
    }
  });

  return (
    <SquareContainer ref={drop} isDark={isDark}>
      {children}
    </SquareContainer>
  );
};

export default Square;

const SquareContainer = styled.div<Pick<BoardSquareProps, 'isDark'>>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 12.5%;
  height: 12.5%;
  background: ${({ isDark, theme }) => (isDark ? theme.chess.squareDark : theme.chess.squareLight)};
`;
