import React from 'react';
import styled from 'styled-components';

type TakenPiecesProps = {
  pieces: string[];
};

const TakenPieces: React.FC<TakenPiecesProps> = ({ pieces }) => {
  return (
    <TakenPiecesContainer>
      {pieces.map((piece, i) => {
        const pieceImg = `/assets/chesspieces/${piece}.png`;
        return (
          <div key={i}>
            <img src={pieceImg} alt={`${i}_${piece}`} />
          </div>
        );
      })}
    </TakenPiecesContainer>
  );
};

export default TakenPieces;

const TakenPiecesContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  padding: 1.5rem 1rem;
  border-radius: 12px;
  align-items: center;
  justify-content: center;
  max-height: 100px;
  overflow-y: scroll;
  gap: 0.5rem;

  img {
    width: 100%;
    height: 100%;
  }
`;
