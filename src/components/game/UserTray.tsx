import { GameMember } from '@/types';
import React from 'react';
import styled from 'styled-components';
import TakenPieces from './TakenPieces';
import { Color } from 'chess.js';

type UserTrayProps = {
  user: Pick<GameMember, 'name' | 'piece'>;
  pieces: string[];
  turn: Color;
};

const UserTray: React.FC<UserTrayProps> = ({ user, pieces, turn }) => {
  return (
    <UserTrayContainer piece={user.piece} turn={turn}>
      <div className="username">{user.name}</div>
      <TakenPieces pieces={pieces} />
    </UserTrayContainer>
  );
};

export default UserTray;

const UserTrayContainer = styled.div<{ piece: Color; turn: Color }>`
  display: flex;
  justify-content: center;
  width: 100%;
  position: relative;
  padding: 2rem;
  border: 2px solid ${({ theme }) => theme.colors.borderPrimary};
  border-radius: 12px;
  box-shadow: 0 10px 25px ${({ theme }) => theme.colors.bgPrimary};

  .username {
    position: absolute;
    top: -20px;
    background: ${({ piece, theme }) =>
      piece === 'w' ? theme.chess.squareLight : theme.chess.squareDark};
    color: ${({ piece }) => (piece === 'w' ? '#000' : '#FFF')};
    padding: 0.5rem;
    border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
    border-radius: 12px;
    transform: ${({ piece, turn }) => (piece === turn ? 'scale(1.2)' : '')};
  }
`;
