import React, { useState } from 'react';
import { Button, FormInput, FormContainer, ThemeSwitch } from '../shared';
import styled from 'styled-components';
import { FaArrowLeft, FaGoogle } from 'react-icons/fa';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { GoogleAuthProvider, signInAnonymously, signInWithPopup } from 'firebase/auth';
import { auth } from '@/utils/firebase';
import { toast } from 'react-hot-toast';

const SignIn: React.FC = () => {
  const { state } = useLocation();
  const location = state ? state.location : '/';
  const navigate = useNavigate();
  const [username, setUsername] = useState<string>();
  const [loading, setLoading] = useState<{ anon: boolean; google: boolean }>({
    anon: false,
    google: false
  });

  const handleSignInWithGoogle = async () => {
    setLoading((prev) => ({ ...prev, google: true }));
    const provider = new GoogleAuthProvider();
    await signInWithPopup(auth, provider)
      .then(() => {
        setLoading((prev) => ({ ...prev, google: false }));
        toast.success('Sign In Successful!');
        setTimeout(() => navigate(location), 1000);
      })
      .catch((error) => {
        setLoading((prev) => ({ ...prev, google: false }));
        toast.error(
          `Failed to sign in: ${error.message || error.error || 'an unknown error occurred'}`
        );
      });
  };

  const handleAnonSignIn = async () => {
    setLoading((prev) => ({ ...prev, anon: true }));
    await signInAnonymously(auth)
      .then(() => {
        setLoading((prev) => ({ ...prev, anon: true }));
        toast.success('Sign In Successful!');
        localStorage.setItem('username', username as string);
        setTimeout(() => navigate(location), 1000);
      })
      .catch((error) => {
        setLoading((prev) => ({ ...prev, anon: true }));
        toast.error(
          `Failed to sign in: ${error.message || error.error || 'an unknown error occurred'}`
        );
      });
  };

  return (
    <SignInContainer>
      <ThemeSwitch />
      <div className="header">
        <h1>Sign In</h1>
      </div>
      <div className="body">
        <Button
          name="signin_google"
          variant="google"
          onClick={handleSignInWithGoogle}
          disabled={loading.anon || loading.google}
          isLoading={loading.google}
        >
          <span>Sign In With Google</span>
          <FaGoogle />
        </Button>
        <div className="or">OR</div>
        <FormContainer>
          <FormInput
            name="username"
            placeholder="Enter Username"
            onChange={({ target: { value } }) => setUsername(value)}
          />
          <Button
            name="signin_anon"
            label="Sign In Anonymously"
            onClick={handleAnonSignIn}
            disabled={loading.anon || loading.google}
            isLoading={loading.anon}
          />
          <Link to="/" className="back-home__link" aria-label="back_home">
            <FaArrowLeft /> <span>Back to Home</span>
          </Link>
        </FormContainer>
      </div>
    </SignInContainer>
  );
};

export default SignIn;

export const SignInContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 40%;
  background: ${({ theme }) => theme.colors.bgPrimary};
  padding: 2rem;
  border-radius: 32px;
  gap: 2rem;
  box-shadow: 0 10px 25px ${({ theme }) => theme.colors.bgPrimary};
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};

  .header {
    display: flex;
    flex-direction: column;
    gap: 1rem;
    text-align: center;

    p {
      color: ${({ theme }) => theme.colors.textSecondary};
    }
  }

  .body {
    width: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    text-align: center;
    gap: 1rem;
    button {
      width: 100%;
    }
    a {
      display: flex;
      align-items: center;
      gap: 5px;
      color: ${({ theme }) => theme.colors.textPrimary};
      transition: 0.3s ease-in-out;
      &:hover {
        svg {
          transform: translateX(-5px);
        }
      }
    }
  }

  .or {
    color: ${({ theme }) => theme.colors.textSecondary};
  }

  @media (max-width: 600px) {
    width: 90%;
  }
`;
