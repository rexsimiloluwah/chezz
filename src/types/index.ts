import { Color, PieceSymbol, Square } from 'chess.js';
import { Timestamp } from 'firebase/firestore';

export type StartingPiece = Color | 'random';

export type GameInitResponse = {
  status: boolean;
  error?: string;
};

export type GameOverReason =
  | 'checkmate'
  | 'stalemate'
  | 'draw'
  | 'insufficient_material'
  | 'threefold_repetition'
  | '50_moves_rule'
  | 'unknown';

export type GameResult = {
  winner?: Color;
  reason: GameOverReason;
};

export type GameStatus = 'waiting' | 'ready' | 'reset';

export type GameMember = {
  uid: string;
  piece: Color;
  name: string;
  isCreator: boolean;
  score: number;
  profileImage: string | null;
};

export type ChessPiece = {
  square: Square;
  color: Color;
  type: PieceSymbol;
};

export type ChessBoard = Array<Array<ChessPiece | null>>;

export type Promotion = { from: string; to: string; color: Color; piece: PieceSymbol };

export type MultiPlayerGameData = {
  status: GameStatus;
  members: Array<GameMember>;
  id: string;
  pendingPromotion: Promotion | null;
  takenPieces: string[];
  isGameOver: boolean;
  result: GameResult | null;
  turn: Color;
  pastResults: GameResult[];
};

export type MultiPlayerGameDocument = MultiPlayerGameData & {
  board: string;
  createdAt: Timestamp;
};

export type MultiplayerGameObservableData = MultiPlayerGameData & { board: ChessBoard };

export type SinglePlayerData = {
  board: ChessBoard;
  pendingPromotion: Promotion | null;
  isGameOver: boolean;
  result: GameResult | null;
  turn: Color;
  takenPieces: string[];
};
