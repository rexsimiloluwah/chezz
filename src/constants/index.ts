import { Color } from 'chess.js';

export const PIECE_SELECT_OPTIONS: Array<{
  value: Color | 'random';
  label: string;
}> = [
  {
    value: 'b',
    label: 'Black'
  },
  {
    value: 'w',
    label: 'White'
  },
  {
    value: 'random',
    label: 'Random'
  }
];

export const BOARD_LETTERS_W = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
export const BOARD_NUMBERS_W = [...Array(8)].map((_, i) => i + 1).reverse();
export const BOARD_LETTERS_B = [...BOARD_LETTERS_W].reverse();
export const BOARD_NUMBERS_B = [...BOARD_NUMBERS_W].reverse();
