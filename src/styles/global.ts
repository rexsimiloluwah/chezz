import { createGlobalStyle } from 'styled-components';
import { DefaultTheme } from './theme';

const GlobalStyles = createGlobalStyle<{ theme: DefaultTheme }>`
    * {
        margin: 0px;
        padding: 0px;
        box-sizing: border-box;
    }

    body{
        background: ${({ theme }) => theme.colors.body};
        transition: background .3s ease-out;
        color: ${({ theme }) => theme.colors.textPrimary}
    }

    a{
        text-decoration: none;
    }

    h1 {
        font-size: 30px;
        @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
            font-size: 28px;
        }
    }
    
    h2 {
        font-size: 22px;
        @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
            font-size: 20px;
        }
    }

    h3 {
        font-size: 18px;
        @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
            font-size: 16px;
        }
    }

    p {
        font-size: 16px;
        @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
            font-size: 14px;
        }
    }
`;

export default GlobalStyles;
