export interface DefaultTheme {
  colors: {
    body: string;
    bgPrimary: string;
    bgSecondary: string;
    primary: string;
    secondary: string;
    google: string;
    info: string;
    danger: string;
    success: string;
    warning: string;
    gray: string;
    grayDark: string;
    white: string;
    textPrimary: string;
    textSecondary: string;
    borderPrimary: string;
    shadow: string;
  };
  chess: {
    squareDark: string;
    squareLight: string;
  };
  breakpoints: {
    xs: string;
    sm: string;
    md: string;
    lg: string;
    xl: string;
  };
}

export const lightTheme: DefaultTheme = {
  colors: {
    danger: '#C62828',
    success: '#00897B',
    textPrimary: '#000000',
    textSecondary: 'rgba(0,0,0,0.9)',
    warning: '#303F9F',
    white: '#FFF',
    gray: '#6C757D',
    grayDark: '#343A40',
    primary: '#007BFF',
    secondary: '#6C757D',
    info: '#17A2B8',
    body: 'rgb(255,255,255)',
    bgPrimary: '#F3F3F3',
    bgSecondary: '#4ECDC4',
    borderPrimary: 'rgba(0,0,0,0.2)',
    google: '#DB4437',
    shadow: '#ccc'
  },
  chess: {
    squareDark: '#76A644',
    squareLight: '#F1F3C2'
  },
  breakpoints: {
    xs: '0px',
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px'
  }
};

export const darkTheme: DefaultTheme = {
  colors: {
    danger: '#C62828',
    success: '#00897B',
    textPrimary: '#FFFFFF',
    textSecondary: 'rgba(255,255,255,0.9)',
    warning: '#303F9F',
    white: '#FFF',
    gray: '#6C757D',
    grayDark: '#343A40',
    primary: '#007BFF',
    secondary: '#6C757D',
    info: '#17A2B8',
    body: '#222222',
    bgPrimary: '#181818',
    bgSecondary: '#4ECDC4',
    borderPrimary: 'rgba(255,255,255,0.2)',
    google: '#DB4437',
    shadow: '#00000A'
  },
  chess: {
    squareDark: '#76A644',
    squareLight: '#F1F3C2'
  },
  breakpoints: {
    xs: '0px',
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px'
  }
};
