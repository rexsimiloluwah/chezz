import React, { PropsWithChildren } from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { auth } from '@/utils/firebase';
import { useAuthState } from 'react-firebase-hooks/auth';
import { PagePreloader } from '@/components/shared';
import { toast } from 'react-hot-toast';

const AuthProtectedRoute: React.FC<PropsWithChildren> = ({ children }) => {
  const location = useLocation();
  const [user, loading, error] = useAuthState(auth);

  if (loading) {
    return <PagePreloader />;
  }

  if (error) {
    toast.error('Auth error');
    return <Navigate to="/auth/login" state={{ location }} />;
  }

  if (!user) {
    return <Navigate to="/auth/login" state={{ location }} />;
  }

  return <div>{children}</div>;
};

export default AuthProtectedRoute;
