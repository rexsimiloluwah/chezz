import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { CreateGame, SignIn, SinglePlayerChess, MultiPlayerChess, Games, Home } from '@/pages';
import { ThemeProvider } from 'styled-components';
import { lightTheme, darkTheme } from '@/styles/theme';
import { useThemeStateContext } from '@/context';
import GlobalStyles from '@/styles/global';
import AuthProtectedRoute from './AuthProtectedRoute';

const Router: React.FC = () => {
  const { theme } = useThemeStateContext();

  return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
      <GlobalStyles />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route
            path="/create-game"
            element={
              <AuthProtectedRoute>
                <CreateGame />
              </AuthProtectedRoute>
            }
          />
          <Route path="/game" element={<SinglePlayerChess />} />
          <Route
            path="/game/:gameId"
            element={
              <AuthProtectedRoute>
                <MultiPlayerChess />
              </AuthProtectedRoute>
            }
          />
          <Route path="auth/login" element={<SignIn />} />
          <Route path="/games" element={<Games />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default Router;
