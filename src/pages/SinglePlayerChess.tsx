import ChessBoard from '@/components/game/chessboard';
import React, { useEffect, useMemo, useState } from 'react';
import { GameContainer } from './styles';
import { SinglePlayerData } from '@/types';
import { singlePlayerGame } from '@/utils/chessgame';
import { Button, Modal, SideDrawer } from '@/components/shared';
import { IModalProps } from '@/components/shared/Modal';
import { GameOver, UserTray } from '@/components/game';
import { GameOverProps } from '@/components/game/GameOver';
import { Color } from 'chess.js';
import { GamePageLayout } from '@/layouts';
import { GameSettingsContainer, SettingsButton } from './MultiPlayerChess';
import { FaCog } from 'react-icons/fa';
import { SideDrawerProps } from '@/components/shared/SideDrawer';

const GameOverModal: React.FC<IModalProps & GameOverProps> = ({ ...props }) => {
  return (
    <Modal {...props}>
      <GameOver {...props} />
    </Modal>
  );
};

type GameOverSettingsProps = {
  handleResetGame: () => void;
};

const GameOverSettingsDrawer: React.FC<SideDrawerProps & GameOverSettingsProps> = ({
  handleResetGame,
  ...props
}) => {
  return (
    <SideDrawer title={'Game Settings'} {...props}>
      <GameSettingsContainer>
        <Button name="reset_board" label="Reset Board" onClick={handleResetGame} />
      </GameSettingsContainer>
    </SideDrawer>
  );
};

const getPieceName = (pieceColor: Color) => {
  return pieceColor === 'w' ? 'White' : 'Black';
};

const SinglePlayerChessPage: React.FC = () => {
  const [gameData, setGameData] = useState<SinglePlayerData | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [isResultModalOpen, setIsResultModalOpen] = useState<boolean>(false);
  const [isSettingsDrawerOpen, setIsSettingsDrawerOpen] = useState<boolean>(false);

  const whiteTakenPieces = useMemo(
    () => gameData?.takenPieces.filter((piece) => piece.split('_')[1] === 'w') || [],
    [gameData]
  );
  const blackTakenPieces = useMemo(
    () => gameData?.takenPieces.filter((piece) => piece.split('_')[1] === 'b') || [],
    [gameData]
  );
  const winner = useMemo(() => {
    return gameData?.result?.winner
      ? {
          name: gameData?.result?.winner === 'w' ? 'White' : 'Black',
          piece: gameData?.result?.winner
        }
      : null;
  }, [gameData]);

  useEffect(() => {
    // for initializing the chess board
    singlePlayerGame.init();
    setLoading(true);
    const subscribe = singlePlayerGame.obsv.subscribe((game) => {
      setGameData(game);
      if (game.result) {
        setIsResultModalOpen(true);
      }
      setLoading(false);
    });

    return () => subscribe.unsubscribe();
  }, []);

  if (loading || !gameData) {
    return <div>Loading....</div>;
  }

  const handleResetBoard = async () => {
    await singlePlayerGame.reset();
    setIsSettingsDrawerOpen(false);
  };

  return (
    <GamePageLayout>
      {gameData.result && (
        <GameOverModal
          isOpen={isResultModalOpen}
          onAfterClose={() => singlePlayerGame.reset()}
          onRequestClose={() => setIsResultModalOpen(false)}
          reason={gameData.result.reason}
          winner={winner}
          isMultiPlayer={false}
        />
      )}

      <SettingsButton
        name="settings"
        title="game_settings"
        onClick={() => setIsSettingsDrawerOpen(true)}
      >
        <FaCog />
      </SettingsButton>

      <GameOverSettingsDrawer
        isOpen={isSettingsDrawerOpen}
        onRequestClose={() => setIsSettingsDrawerOpen(false)}
        handleResetGame={handleResetBoard}
      />

      <GameContainer>
        <div>
          <UserTray
            user={{
              name: getPieceName('w'),
              piece: 'w'
            }}
            pieces={whiteTakenPieces}
            turn={gameData.turn}
          />
        </div>
        <div>
          <ChessBoard
            board={gameData.board}
            turn={winner ? winner.piece : gameData.turn}
            isMultiPlayer={false}
          />
          {/* <Button name="reset_board" label="Reset Board" onClick={()=>singlePlayerGame.reset()}/> */}
        </div>
        <div>
          <UserTray
            user={{
              name: getPieceName('b'),
              piece: 'b'
            }}
            pieces={blackTakenPieces}
            turn={gameData.turn}
          />
        </div>
      </GameContainer>
    </GamePageLayout>
  );
};

export default SinglePlayerChessPage;
