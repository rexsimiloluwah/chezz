import { CenterPageLayout } from '@/layouts';
import React from 'react';
import { CreateGame } from '@/components/game';

const CreateGamePage: React.FC = () => {
  return (
    <CenterPageLayout>
      <CreateGame />
    </CenterPageLayout>
  );
};

export default CreateGamePage;
