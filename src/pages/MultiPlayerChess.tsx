import React, { useState, useEffect, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { GameOver, UserTray } from '@/components/game';
import { Modal, SideDrawer, Button, PagePreloader } from '@/components/shared';
import { IModalProps } from '@/components/shared/Modal';
import { GameOverProps } from '@/components/game/GameOver';
import { GameInitResponse, GameMember, GameResult, MultiplayerGameObservableData } from '@/types';
import { auth } from '@/utils/firebase';
import { Subscription } from 'rxjs';
import { multiPlayerGame } from '@/utils/chessgame';
import { GameContainer } from './styles';
import ChessBoard from '@/components/game/chessboard';
import { Color } from 'chess.js';
import { GamePageLayout } from '@/layouts';
import { FaCog } from 'react-icons/fa';
import styled from 'styled-components';
import { SideDrawerProps } from '@/components/shared/SideDrawer';

type GameOverSettingsProps = {
  pastResults: GameResult[];
  gameMembers: GameMember[];
  handleResetGame: () => void;
};

const GameOverModal: React.FC<IModalProps & GameOverProps> = ({ ...props }) => {
  return (
    <Modal {...props}>
      <GameOver {...props} />
    </Modal>
  );
};

const GameOverSettingsDrawer: React.FC<SideDrawerProps & GameOverSettingsProps> = ({
  pastResults,
  gameMembers,
  handleResetGame,
  ...props
}) => {
  return (
    <SideDrawer title={'Game Settings'} {...props}>
      <GameSettingsContainer>
        <div>
          <h2>Games Played: {pastResults.length}</h2>
        </div>
        <div>
          <h2>Scores</h2>
          <ScoreContainer>
            {gameMembers.map((member, i) => (
              <div key={i} className={`score ${member.piece === 'w' ? 'score--w' : 'score--b'}`}>
                <span>{member.name}</span>
                <span className="value">{member.score}</span>
              </div>
            ))}
          </ScoreContainer>
        </div>
        <div>
          <Button name="reset_board" label="Reset Board" onClick={handleResetGame} />
        </div>
      </GameSettingsContainer>
    </SideDrawer>
  );
};

const MultiPlayerChessPage: React.FC = () => {
  const [gameData, setGameData] = useState<MultiplayerGameObservableData | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [initResult, setInitResult] = useState<GameInitResponse>();
  const [isResultModalOpen, setIsResultModalOpen] = useState<boolean>(false);
  const [isSettingsDrawerOpen, setIsSettingsDrawerOpen] = useState<boolean>(false);
  const { currentUser } = auth;
  const { gameId } = useParams();

  const player = useMemo(
    () => gameData?.members.find((member) => member.uid === currentUser?.uid),
    [gameData]
  );
  const opponent = useMemo(
    () => gameData?.members.find((member) => member.uid !== currentUser?.uid),
    [gameData]
  );
  const winner = useMemo(() => {
    const winnerPiece = gameData?.result?.winner;
    if (!winnerPiece) {
      return null;
    }

    // get the user who won
    const winnerUser = gameData.members.find((member) => member.piece === gameData.result?.winner);
    return winnerPiece
      ? {
          name: winnerUser?.name as string,
          piece: winnerPiece
        }
      : null;
  }, [gameData]);
  const playerTakenPieces = useMemo(
    () => gameData?.takenPieces.filter((piece) => piece.split('_')[1] === player?.piece) || [],
    [gameData]
  );
  const opponentTakenPieces = useMemo(
    () => gameData?.takenPieces.filter((piece) => piece.split('_')[1] === opponent?.piece) || [],
    [gameData]
  );

  const handleResetBoard = async () => {
    await multiPlayerGame.reset();
    setIsSettingsDrawerOpen(false);
  };

  useEffect(() => {
    let subscribe: Subscription;
    // for initializing the chess board
    const initGame = async () => {
      if (!currentUser) return;

      setLoading(true);

      const initResponse = await multiPlayerGame.init(gameId as string, {
        uid: currentUser?.uid,
        name: (currentUser.displayName || localStorage.getItem('username')) as string,
        profileImage: currentUser.photoURL || null
      });
      setInitResult(initResponse);

      if (!initResponse.error) {
        subscribe = multiPlayerGame.obsv.subscribe(async (game) => {
          const gameData = await game;
          setGameData(gameData);
          setLoading(false);
          if (gameData.result) {
            setIsResultModalOpen(true);
          }
        });
      } else {
        setLoading(false);
      }
    };

    initGame();

    return () => subscribe && subscribe.unsubscribe();
  }, [gameId]);

  if (loading || !gameData) {
    return <PagePreloader />;
  }

  if (initResult?.error) {
    return <div>{initResult.error}</div>;
  }
  return (
    <GamePageLayout>
      <SettingsButton
        name="settings"
        title="game_settings"
        onClick={() => setIsSettingsDrawerOpen(true)}
      >
        <FaCog />
      </SettingsButton>

      <GameOverSettingsDrawer
        isOpen={isSettingsDrawerOpen}
        onRequestClose={() => setIsSettingsDrawerOpen(false)}
        pastResults={gameData.pastResults}
        gameMembers={gameData.members}
        handleResetGame={handleResetBoard}
      />

      {gameData.result && (
        <GameOverModal
          isOpen={isResultModalOpen}
          onAfterClose={() => multiPlayerGame.reset()}
          onRequestClose={() => setIsResultModalOpen(false)}
          reason={gameData.result.reason}
          winner={winner}
          isWinner={winner?.piece === player?.piece}
          isMultiPlayer={true}
        />
      )}
      <GameContainer>
        <div>
          {player && <UserTray user={player} pieces={playerTakenPieces} turn={gameData.turn} />}
        </div>
        <div>
          <ChessBoard board={gameData.board} turn={player?.piece as Color} isMultiPlayer={true} />
        </div>
        <div>
          {opponent && (
            <UserTray user={opponent} pieces={opponentTakenPieces} turn={gameData.turn} />
          )}
        </div>
      </GameContainer>
    </GamePageLayout>
  );
};

export default MultiPlayerChessPage;

export const SettingsButton = styled.button`
  position: absolute;
  right: 24px;
  top: 12px;
  color: ${({ theme }) => theme.colors.textPrimary};
  font-size: 32px;
  box-shadow: none;
  background: none;
  border: none;
  cursor: pointer;
  z-index: 99;
`;

export const GameSettingsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 1rem;

  > div {
    width: 100%;
    padding: 0.5rem 1rem;
    border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
    border-radius: 12px;
    box-shadow: 0 2px 3px ${({ theme }) => theme.colors.shadow};
  }
`;

const ScoreContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 1rem;
  margin: 0.5rem 0;

  .score {
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-radius: 12px;
    border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
    padding: 0.5rem;
  }

  .score .value {
    font-size: 25px;
    font-weight: 600;
  }

  .score--w {
    background: ${({ theme }) => theme.chess.squareLight};
    color: #000;
  }

  .score--b {
    background: ${({ theme }) => theme.chess.squareDark};
  }
`;
