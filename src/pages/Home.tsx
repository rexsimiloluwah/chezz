import { Button } from '@/components/shared';
import { CenterPageLayout } from '@/layouts';
import { auth } from '@/utils/firebase';
import { signOut } from 'firebase/auth';
import React from 'react';
import { FaChessPawn, FaChessRook } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import { useAuthState } from 'react-firebase-hooks/auth';
import { toast } from 'react-hot-toast';

const Home: React.FC = () => {
  const navigate = useNavigate();

  const [user] = useAuthState(auth);

  const handleSignOut = async () => {
    await signOut(auth)
      .then(() => {
        toast.success('Sign Out Successful!');
      })
      .catch((error) => {
        toast.error(
          `Failed to sign out: ${error.message || error.error || 'an unknown error occurred'}`
        );
      });
  };
  return (
    <CenterPageLayout>
      <HomeContainer>
        {user && (
          <Button
            name="sign_out"
            variant="danger"
            label="Sign Out"
            className="signout__btn"
            onClick={handleSignOut}
          />
        )}
        <div className="header">
          <Heading>Chezz</Heading>
          <p>Welcome to Chezz</p>
        </div>
        <div className="body">
          <Button name="home_create_game" onClick={() => navigate('/create-game')}>
            <span>Create New Multi-player Game</span>
            <FaChessPawn />
          </Button>
          <Button name="home_view_games" variant="primary" onClick={() => navigate('/game')}>
            <span>Create New Single Player Game</span>
            <FaChessRook />
          </Button>
        </div>
      </HomeContainer>
    </CenterPageLayout>
  );
};

export default Home;

const Heading = styled.div`
  font-size: 32px;
  font-weight: bold;
`;

const HomeContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 1.5rem;
  width: 45%;

  .header,
  .body {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 1rem;
    width: 100%;
  }

  .body button {
    width: 100%;
  }

  .signout__btn {
    position: absolute;
    top: 12px;
    right: 12px;
    padding: 0.5rem;
  }
`;
