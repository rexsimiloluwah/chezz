import styled from 'styled-components';

export const GameContainer = styled.div`
  min-height: 100vh;
  display: grid;
  align-items: center;
  grid-template-columns: 1.5fr 4fr 1.5fr;
  row-gap: 1rem;
  padding: 0 1rem;
  position: relative;

  > div {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }

  > div:nth-child(2) {
    height: 100vh;
    width: 100%;
  }
`;
