import React from 'react';
import { SignIn } from '@/components/game';
import { CenterPageLayout } from '@/layouts';

const SignInPage: React.FC = () => {
  return (
    <CenterPageLayout>
      <SignIn />
    </CenterPageLayout>
  );
};

export default SignInPage;
