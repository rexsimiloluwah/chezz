import React, { useEffect, useState } from 'react';
import { collection, CollectionReference, onSnapshot } from 'firebase/firestore';
import { db } from '@/utils/firebase';
import { MultiPlayerGameDocument } from '@/types';
import { Button } from '@/components/shared';
import styled from 'styled-components';
import { formatTimeDifference } from '@/utils/common';
import ProfileImage from '@/components/shared/ProfileImage';

const Games: React.FC = () => {
  const [games, setGames] = useState<Array<MultiPlayerGameDocument>>([]);

  useEffect(() => {
    const unsubscribe = onSnapshot<MultiPlayerGameDocument>(
      collection(db, 'games') as CollectionReference<MultiPlayerGameDocument>,
      (dbRef) => {
        const availableGames: MultiPlayerGameDocument[] = [];
        dbRef.forEach((doc) => {
          const gameData = doc.data();
          if (gameData.status === 'waiting' && !availableGames.includes(gameData)) {
            availableGames.push(gameData);
          }
        });
        setGames(availableGames);
      }
    );

    return () => unsubscribe();
  }, []);

  return (
    <GamesContainer>
      <div className="header">
        <h1>Available Games</h1>
        <p>Join any of these available games to play against a random opponent.</p>
      </div>
      <div className="body">
        {games.map((game, id) => {
          const creator = game.members.find((member) => member.isCreator);

          return (
            <GameCard key={id}>
              <div className="picture">
                <ProfileImage imgSrc={creator?.profileImage || null} username={creator?.name} />
              </div>
              <div className="timestamp">{formatTimeDifference(game.createdAt.toDate())}</div>
              <Button name="join_game" label={`Play against ${creator?.name}`} />
            </GameCard>
          );
        })}
      </div>
    </GamesContainer>
  );
};

export default Games;

const GamesContainer = styled.div`
  width: 80%;
  margin: 2rem auto;

  .header,
  .body {
    display: flex;
    flex-direction: column;
    gap: 1rem;
    padding: 1rem 0;
  }

  .header {
    border-bottom: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  }

  .body {
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    width: 95%;
  }
`;

const GameCard = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  justify-content: center;
  align-items: center;
  padding: 1rem;
  border: 1px solid ${({ theme }) => theme.colors.borderPrimary};
  border-radius: 12px;
  box-shadow: 0 2px 3px ${({ theme }) => theme.colors.shadow};

  .timestamp {
    color: ${({ theme }) => theme.colors.textSecondary};
  }
`;
