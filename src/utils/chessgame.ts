import {
  GameInitResponse,
  GameMember,
  GameOverReason,
  GameResult,
  MultiPlayerGameDocument,
  MultiplayerGameObservableData,
  Promotion,
  SinglePlayerData
} from '@/types';
import { Chess, Move } from 'chess.js';
import { doc, DocumentReference, getDoc, setDoc, Timestamp, updateDoc } from 'firebase/firestore';
import { fromRef } from 'rxfire/firestore';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { v4 } from 'uuid';
import { db } from './firebase';

class SinglePlayerGame {
  private _chess: Chess;
  obsv: BehaviorSubject<SinglePlayerData>;
  takenPieces: string[];

  constructor() {
    this._chess = new Chess();
    this.obsv = new BehaviorSubject<SinglePlayerData>({
      board: this._chess.board(),
      pendingPromotion: null,
      isGameOver: false,
      result: null,
      turn: this._chess.turn(),
      takenPieces: []
    });
    this.takenPieces = [];
  }

  private updateGame(pendingPromotion: Promotion | null, move?: Move) {
    const { _chess, takenPieces } = this;

    const isGameOver = _chess.isGameOver();

    let newTakenPieces = takenPieces;
    // update the list of taken pieces if the move leads to the capture of a piece
    if (move?.captured) {
      const takenPiece = `${move?.captured}_${_chess.turn()}`;
      newTakenPieces = [...takenPieces, takenPiece];
    }

    this.takenPieces = newTakenPieces;

    const newGameState = {
      board: _chess.board(),
      pendingPromotion,
      isGameOver,
      result: isGameOver ? getGameResult(_chess) : null,
      turn: _chess.turn(),
      takenPieces: newTakenPieces
    };

    localStorage.setItem('savedGame', _chess.fen());
    localStorage.setItem('takenPieces', JSON.stringify(takenPieces));

    this.obsv.next(newGameState);
  }

  public init() {
    const { _chess } = this;
    // fetch the saved game state from local storage
    const savedGame = localStorage.getItem('savedGame');
    // fetch the saved taken pieces state from local storage
    const savedTakenPieces = localStorage.getItem('takenPieces');

    if (savedGame) {
      _chess.load(savedGame);
    }

    const gameValue = {
      ...this.obsv.value,
      board: _chess.board(),
      takenPieces: !savedTakenPieces || savedTakenPieces === '' ? [] : JSON.parse(savedTakenPieces)
    };
    this.obsv.next(gameValue);
  }

  private move(from: string, to: string): Move | undefined {
    console.log('making a move...');
    let legalMove: Move;
    const { _chess } = this;
    try {
      legalMove = _chess.move({ from, to });

      if (legalMove) {
        this.updateGame(null, legalMove);
      }
      return legalMove;
    } catch (error) {
      console.log(error);
      return;
    }
  }

  public handleMove(from: string, to: string) {
    const { _chess } = this;
    const moves = _chess.moves({ verbose: true });
    const promotions = moves.filter((m) => m.promotion);
    let pendingPromotion;

    const isPendingPromotion = promotions.some((p) => `${p.from}:${p.to}` === `${p.from}:${p.to}`);

    // check if there is a pending promotion
    if (isPendingPromotion) {
      // update the game's pending promotion
      pendingPromotion = { from, to, color: _chess.turn(), piece: promotions[0].piece };
      this.updateGame(pendingPromotion);
    }

    if (!pendingPromotion) {
      // make the move
      const m = this.move(from, to);
      return m !== undefined;
    }
    return true;
  }

  public reset() {
    this._chess.reset();
    localStorage.setItem('takenPieces', JSON.stringify([]));
    this.updateGame(null);
  }
}

class MultiPlayerGame {
  private _chess: Chess;
  gameRef: DocumentReference<MultiPlayerGameDocument> | null;
  obsv: Observable<Promise<MultiplayerGameObservableData>>;
  user: GameMember | null;

  constructor() {
    this._chess = new Chess();
    this.gameRef = null;
    this.obsv = new Observable();
    this.user = null;
  }

  /**
   * Create a new game
   * @param creator
   * @returns
   */
  public async create(creator: GameMember): Promise<string> {
    const gameId = v4();
    const newGame = {
      status: 'waiting',
      members: [creator],
      id: gameId,
      takenPieces: [],
      result: null,
      pendingPromotion: null,
      isGameOver: false,
      pastResults: [],
      createdAt: Timestamp.fromDate(new Date())
    };
    this.user = creator;
    await setDoc(doc(db, 'games', gameId), newGame);
    return gameId;
  }

  public async init(
    gameId: string,
    user: Pick<GameMember, 'uid' | 'name' | 'profileImage'>
  ): Promise<GameInitResponse> {
    const { _chess } = this;
    // check if the game exists
    const gameRef = doc(db, 'games', gameId) as DocumentReference<MultiPlayerGameDocument>;
    this.gameRef = gameRef;

    // fetch the game data
    const gameSnapshot = await getDoc(gameRef);
    if (gameSnapshot.exists()) {
      const gameData = gameSnapshot.data();

      // check if the current user is the creator
      const creator = gameData.members.find((m) => m.isCreator === true);
      if (gameData.status === 'waiting' && creator?.uid !== user.uid) {
        // create a new game member
        const newUser: GameMember = {
          ...user,
          isCreator: false,
          piece: creator?.piece === 'w' ? 'b' : 'w',
          score: 0
        };
        this.user = newUser;
        const updatedMembers = [...gameData.members, newUser];

        // update the database
        await updateDoc(gameRef, {
          members: updatedMembers,
          status: 'ready'
        });
      } else if (!gameData.members.map((m) => m.uid).includes(user.uid as string)) {
        return {
          status: false,
          error: 'Game is full!'
        };
      }

      // reset the game
      _chess.reset();

      // set the user
      if (!this.user) {
        this.user = gameData.members.find((member) => (member.uid = user.uid)) as GameMember;
      }

      // create the observable for real-time updates from the firestore collection
      this.obsv = fromRef(gameRef).pipe(
        map(async (gameDoc) => {
          const gameData = await gameDoc.data();
          const {
            pendingPromotion,
            board,
            members,
            id,
            status,
            takenPieces,
            isGameOver,
            result,
            pastResults
          } = gameData as MultiPlayerGameDocument;

          if (board) {
            _chess.load(board);
          }

          return {
            id,
            board: _chess.board(),
            takenPieces,
            pendingPromotion,
            isGameOver,
            members,
            turn: _chess.turn(),
            result,
            status,
            pastResults
          };
        })
      );
      return {
        status: true
      };
    } else {
      return {
        status: false,
        error: 'Game does not exist!'
      };
    }
  }

  private async updateGame(
    pendingPromotion: { from: string; to: string; color: string } | null,
    move?: Move,
    reset?: boolean
  ) {
    const { _chess, gameRef } = this;
    if (gameRef) {
      const gameSnapshot = await getDoc(gameRef);
      const { takenPieces, status, members, pastResults } =
        gameSnapshot.data() as MultiPlayerGameDocument;
      let newTakenPieces = takenPieces;

      // update the list of taken pieces if the move leads to the capture of a piece
      if (move?.captured) {
        const takenPiece = `${move?.captured}_${_chess.turn()}`;
        newTakenPieces = [...takenPieces, takenPiece];
      }

      const isGameOver = _chess.isGameOver();

      let gameResult: GameResult | null = null;
      let gameResults = pastResults;

      if (isGameOver) {
        gameResult = getGameResult(_chess);
        if (gameResult.reason === 'checkmate') {
          // update the winner's score
          const winnerIdx = members.findIndex((member) => member.piece === gameResult?.winner);
          members[winnerIdx].score += 1;
        }

        // update the game results array
        gameResults = [...pastResults, gameResult];
      }

      const updatedData = {
        board: _chess.fen(),
        turn: _chess.turn(),
        takenPieces: newTakenPieces,
        pendingPromotion: pendingPromotion,
        status: reset ? 'reset' : status,
        isGameOver,
        members: members,
        result: isGameOver ? getGameResult(_chess) : null,
        pastResults: gameResults
      };

      // update the firestore database
      await updateDoc(gameRef, updatedData);
    } else {
      return;
    }
  }

  private async move(from: string, to: string): Promise<Move | undefined> {
    let legalMove: Move;
    const { _chess, user } = this;
    try {
      // check if it is the current user's turn to play
      console.log(user?.piece, _chess.turn());
      if (user?.piece === _chess.turn()) {
        legalMove = _chess.move({ from, to });
      } else {
        return;
      }

      if (legalMove) {
        // update the game data in the firestore collection
        this.updateGame(null, legalMove);
      }
      return legalMove;
    } catch (error) {
      return;
    }
  }

  public async handleMove(from: string, to: string): Promise<boolean> {
    const { _chess } = this;
    const moves = _chess.moves({ verbose: true });
    const promotions = moves.filter((m) => m.promotion);
    let pendingPromotion;

    const isPendingPromotion = promotions.some((p) => `${p.from}:${p.to}` === `${p.from}:${p.to}`);

    // check if there is a pending promotion
    if (isPendingPromotion) {
      // update the game's pending promotion
      pendingPromotion = { from, to, color: promotions[0].color };
      await this.updateGame(pendingPromotion);
    }

    if (!pendingPromotion) {
      // make the move
      const m = await this.move(from, to);
      return m !== undefined;
    }
    return true;
  }

  public async reset(): Promise<void> {
    this._chess.reset();
    await this.updateGame(null, undefined, true);
  }
}

const getGameResult = (game: Chess): GameResult => {
  // check if the game is in checkmate
  if (game.inCheck()) {
    return {
      winner: game.turn() === 'w' ? 'b' : 'w',
      reason: 'checkmate'
    };
  } else if (game.isDraw()) {
    if (game.isStalemate()) {
      return {
        reason: 'stalemate'
      };
    } else if (game.isInsufficientMaterial()) {
      return {
        reason: 'insufficient_material'
      };
    } else if (game.isThreefoldRepetition()) {
      return {
        reason: 'threefold_repetition'
      };
    } else {
      return {
        reason: 'draw'
      };
    }
  } else {
    return {
      reason: 'unknown'
    };
  }
};

export const parseGameOverReason = (reason: GameOverReason, isWinner?: boolean) => {
  switch (reason) {
    case 'checkmate':
      return {
        label: 'Checkmate',
        emoji: isWinner ? '🎉' : '😤'
      };
    case 'stalemate':
      return {
        label: 'Stalemate',
        emoji: '😤'
      };
    default:
      return {
        label: reason,
        emoji: '😐'
      };
  }
};

export const multiPlayerGame = new MultiPlayerGame();
export const singlePlayerGame = new SinglePlayerGame();
