import { Color } from 'chess.js';

type Position = {
  x: number;
  y: number;
};

/**
 * Computes the (x,y) position of a piece from its index
 * @param i
 * @returns
 */
export const getXYPosition = (i: number, pieceColor?: Color): Position => {
  const x = pieceColor === 'b' ? Math.abs((i % 8) - 7) : i % 8;
  const y = pieceColor === 'b' ? Math.floor(i / 8) : Math.abs(Math.floor(i / 8) - 7);
  return {
    x,
    y
  };
};

/**
 * Computes the position of the piece on the board
 * @param i
 * @param pieceColor
 * @returns
 */
export const getBoardPosition = (i: number, pieceColor: Color) => {
  const { x, y } = getXYPosition(i, pieceColor);
  const letter = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'][x];
  return `${letter}${y + 1}`;
};

/**
 * Checks if the current square is dark or light
 * @param i
 * @returns
 */
export const isSquareDark = (i: number) => {
  const { x, y } = getXYPosition(i);
  return (x + y) % 2 === 1;
};
