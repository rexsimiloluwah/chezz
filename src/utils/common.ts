/**
 * Convert milliseconds to human readable string
 * @param value
 * @returns
 */
export const convertMillisToText = (value: number) => {
  const timeBins = [60, 60, 24, 7, 52];
  const timeBinsText = ['second', 'minute', 'hour', 'day', 'week'];

  let count = Math.round(value / 1000);
  for (let i = 0; i < timeBins.length; i++) {
    const rem = count / timeBins[i];

    if (Math.round(rem) === 0) {
      return `${Math.round(count)} ${timeBinsText[i]}${Math.round(count) > 1 ? 's' : ''}`;
    }
    count = rem;
  }

  return `${Math.round(count)} year${Math.round(count) > 1 ? 's' : ''}`;
};

/**
 * Compute the time difference between 'createdAt' and now
 * @param createdAt
 * @returns
 */
export const formatTimeDifference = (createdAt: Date): string => {
  const currentTime = Date.now();
  const timeDifference = currentTime - createdAt.getTime();
  console.log(timeDifference);
  return timeDifference > 0 ? `${convertMillisToText(timeDifference)} ago` : 'Just now';
};
