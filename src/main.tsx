import React from 'react';
import ReactDOM from 'react-dom/client';
import Router from '@/router';
import { ThemeStateProvider, UserProvider } from '@/context';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <DndProvider backend={HTML5Backend}>
      <ThemeStateProvider>
        <UserProvider>
          <Router />
        </UserProvider>
      </ThemeStateProvider>
    </DndProvider>
  </React.StrictMode>
);
